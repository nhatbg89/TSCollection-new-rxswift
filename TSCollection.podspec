Pod::Spec.new do |s|
  s.name         = "TSCollection"
  s.version      = "0.0.1"
  s.summary      = "Common component in Swift"
  s.description  = "Common component in Swift, be collected via outsource projects"
  s.homepage     = "https://bitbucket.org/dashboard/overview"
  s.license      = { :type => "MIT", :file => "License.md" }
  s.author                = { "Sang Ha" => "vansang.dhbkhn@gmail.com" }
  s.social_media_url      = "http://twitter.com/cupidviet"
  s.ios.deployment_target = '9.0'
  s.source                = { :git => "", :tag => s.version }
  s.pod_target_xcconfig   = { 'SWIFT_VERSION' => '3' }
  s.requires_arc          = true
  s.default_subspec       = "Core"

  s.subspec "Core" do |ss|
    ss.source_files   = "Source/Core/*swift"
    ss.dependency       "Then", "~> 2.0"
    ss.framework      = "Foundation"
  end

  s.subspec "AlertManager" do |ss|
    ss.source_files  = "Source/AlertManager/*.swift"
    ss.dependency      "TSCollection/Core"
    ss.dependency      "SDCAlertView", "~> 8.0"
  end

  s.subspec "ApiManager" do |ss|
    ss.source_files  = "Source/ApiManager/*swift"
    ss.dependency      "TSCollection/Core"
    ss.dependency      "Alamofire", "~> 4.0"
    ss.dependency      "Moya/RxSwift", "~> 10.0"
    ss.dependency      "MoyaSugar/RxSwift", "~> 1.0"
    ss.dependency      "ObjectMapper", "~> 3.0"
    ss.dependency      "Result", "~> 3.0"
    ss.dependency      "RxSwift", "~> 4.0"
    ss.dependency      "SwiftyJSON", "~> 4.0"
    ss.framework     = "Foundation"
  end

  s.subspec "Extension" do |ss|
    ss.source_files  = "Source/Extension/**/*.swift"
    ss.dependency      "TSCollection/Core"
    ss.framework     = "UIKit"
  end

  s.subspec "Formatter" do |ss|
    ss.source_files  = "Source/Formatter/**/*.swift"
    ss.dependency      "TSCollection/Core"
  end

  s.subspec "Service" do |ss|
    ss.source_files  = "Source/Service/*.swift"
    ss.dependency      "TSCollection/Core"
    ss.framework     = "UIKit"
    ss.framework     = "CoreLocation"
  end

  s.subspec "ImageManager" do |ss|
    ss.source_files  = "Source/ImageManager/*.swift"
    ss.dependency      "TSCollection/Core"
    ss.dependency      "Kingfisher", "~> 4.0"
  end

  s.subspec "SubviewClass" do |ss|
    ss.source_files  = "Source/SubviewClass/**/*.swift"
    ss.dependency      "TSCollection/Core"
    ss.dependency      "Cartography", "~> 3.0"
    ss.dependency      "RxCocoa", "~> 4.0"
    ss.framework     = "UIKit"
  end
end
