//
//  TSBaseTableViewCell.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import UIKit

open class TSBaseTableViewCell: UITableViewCell {

    deinit {

    }

    open weak var delegate: AnyObject?
    open var indexPath: IndexPath?

    override public init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.configView()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override open func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        self.configView()
    }

    override open func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    open func setIndexPath(indexPath: IndexPath?, sender: AnyObject?) {
        self.indexPath = indexPath
        self.delegate = sender
    }
}

@objc extension TSBaseTableViewCell {

    open func configView() {

    }
}

@objc public extension TSBaseTableViewCell {

    // MARK: - Reuse identifer

    class var identifier: String {
        get {
            let mirror = Mirror(reflecting: self)
            return "\(String(describing: mirror.subjectType).replacingOccurrences(of: ".Type", with: ""))ID"
        }
    }
}

@objc extension TSBaseTableViewCell: TSCellDatasource {

    // MARK: - TSCellDatasource
    
    open class var cellIdentifier: String {
        get {
            return "CellIdentifier"
        }
    }

    open func configCellWithData(data: Any?) {

    }
}
