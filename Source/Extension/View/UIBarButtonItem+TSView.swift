//
//  UIBarButtonItem+TSView.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation

/** TSView Extends UIBarButtonItem

*/
public extension UIBarButtonItem {

    public var itemView: UIView? {
        get {
            return self.value(forKey: "view") as? UIView
        }
    }

    public var itemFrame: CGRect {
        get {
            return self.itemView?.frame ?? CGRect.zero
        }
    }

    public var hidden: Bool {
        get {
            return self.itemView?.isHidden ?? false
        }
        set(value) {
            self.itemView?.isHidden = value
        }
    }
}
