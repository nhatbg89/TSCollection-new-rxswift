//
//  String+TSNumber.swift
//  TSCollection
//
//  Created by Jimmy on 11/5/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation

/** TSNumber Extends String
 
 */
public extension String {
    
    
}

public extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
}
